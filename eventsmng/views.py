from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.
from django.urls import reverse
from django.views import generic, View

from .models import Eventt, Category


# from actions.utils import create_action
# class home(generic.TemplateView):
#     template_name = "eventsmng/home.html"
#
#     def get_context_data(self, **kwargs):
#         context = super(home, self).get_context_data(**kwargs)
#         context['events'] = Eventt.objects.all()
#         context['categories'] = Category.objects.all()
#         return context


class EventtList(generic.ListView):
    model = Eventt
    template_name = 'eventsmng/EventList.html'
    context_object_name = 'events'

    def get_queryset(self):
        try:

            category = self.kwargs["category"]
            events = Category.objects.filter(name=category[:len(category) - 1]).first().events.all()
        except KeyError:
            events = Eventt.objects.all()

        return events

    def get_context_data(self, **kwargs):
        print(kwargs)
        context = super(EventtList, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        return context


#  e = Eventt.objects.all()
#    q = c.union(e)
#   return c, e


#   def events_list(self, category_slug=None):
#      category = None
#       categories = Category.objects.all()
#      events = Eventt.objects.all()
#     if category_slug:
#         category = get_object_or_404(Category, slug=category_slug)
#        events = Eventt.filter(category=category)
#    return render(self, 'Eventlist.html', {'categories': categories,
#                                          'category': category,
#                                         'events': events,
#                                        })


#       return Eventt.objects.filter().order_by('title')[:8]

#       return Eventt.objects.filter(date=timezone.now()).order_by('date')[:8]

class Categorylist(generic.ListView):
    model = Category
    template_name = 'eventsmng/categoryList.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.objects.all()




#  def get_context_data(self,  **kwargs):
#     context = super(Categorylist, self).get_context_data(**kwargs)
#    category = get_object_or_404(Category, slug=category_name)
#   events = Eventt.filter(category=category)
#  return context

# def get_by_category(self, category):
#   return self.filter(category__name=category)


class EventtDisplay(generic.DetailView):
    model = Eventt
    context_object_name = 'event'
    template_name = 'eventsmng/event-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


#    def get_login_url(self):
#        return redirect('eventsmng:login')
# def get_success_url(self):
#    return reverse_lazy('eventsmng:event-detail', kwargs={'pk': self.get_object(Eventt.objects.all()).pk})


class EventtDetail(View):
    # model =Eventt
    def get(self, request, *args, **kwargs):
        view = EventtDisplay.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = EventtCreate.as_view()
        return view(request, *args, **kwargs)


# class EventtFormMixin(object):
#    def form_valid(self, form):
#        form.instance = form.save(commit=False)
#        form.instance.creator = self.request.user
#       create_action(self.request.user, 'created a new event', form.instance)
#       return super().form_valid(form)


class Categorydetail(generic.TemplateView):
    template_name = 'eventsmng/Eventlist.html'

    def get_context_data(self, **kwargs):
        context = super(Categorydetail, self).get_context_data(**kwargs)
        context['events'] = Category.events.all()
        return context


class EventtCreate(LoginRequiredMixin, generic.CreateView):
    #   login_url = '/login/login'
    model = Eventt
    context_object_name = 'event'
    template_name = 'eventsmng/event-create.html'
    fields = ('category', 'title', 'detail', 'date')

    #  success_url = '/eventsmng'

    def get_success_url(self):
        # return super().get_success_url()
        return reverse('eventsmng:event-list')

    #   success_message = "%(title)s was created successfully"

    def form_valid(self, form):
        form.instance = form.save(commit=False)
        form.instance.creator = self.request.user
        form.save()
        return super().form_valid(form)


#    def get_success_url(self):
#      return reverse_lazy('eventsmng:event-create')


class EventtUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Eventt
    template_name = 'eventsmng/event-update.html'
    context_object_name = 'event'
    fields = ('category', 'title', 'detail', 'date',)

    #   success_url = '/.... success update..../'

    def get_success_url(self):
        # return super().get_success_url()
        return reverse('eventsmng:event-detail', args=(self.object.id,))

    #    success_message = "%(title)s was updated successfully"

    def form_valid(self, form):
        form.instance = form.save(commit=False)
        form.instance.creator = self.request.user
        form.save()
        return super().form_valid(form)


class EventtDelete(LoginRequiredMixin, generic.DeleteView):
    model = Eventt
    context_object_name = 'event'
    template_name = 'eventsmng/delete.html'

    def get_success_url(self):
        return reverse('eventsmng:event-list', )

#    success_message = "%(title)s was deleted successfully"


#   def get_success_url(self):
#      return reverse_lazy('eventsmng:event-list')
