# Create your models here.
import self as self
from django.conf import settings
from django.db import models
# Create your models here.
from django.urls import reverse
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(null=True , unique=True)

    #    slug = models.SlugField(unique=True)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(args, kwargs)
    #     self.slug = None

    def __str__(self):
        return self.name

    def get_absolute_url(self):

        return reverse('eventsmng:events_by_category', kwargs={'slug': self.slug})

    # def get_absolute_url(self):
    #     print('------>>', self.name)
    #
    #     return reverse('eventsmng:category_list),', args=[self.name])
    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'


class Eventt(models.Model):
    title = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='events')
    detail = models.TextField(default=' ')
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.title

    #   def get_absolute_url(self):
    #      return reverse('event-list')

    class Meta:
        verbose_name = 'event'
        verbose_name_plural = 'events'
