from django import forms

from .models import Eventt


class EventForm(forms.ModelForm):
    class Meta:
        model = Eventt
        fields = ('category', 'title', 'details', 'date',)
