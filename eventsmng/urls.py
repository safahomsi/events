from django.urls import path, re_path
from . import views
from django.views.generic import TemplateView

from .views import Categorydetail
app_name = 'eventsmng'

urlpatterns = [
    #re_path('list/(?P<category>.+)?', views.EventtList.as_view(), name='event-list'),
    #path('list/', views.EventtList.as_view(), name='event_list'),
    path('events/<slug:category_slug>', views.EventtList.as_view, name='events_by_category'),
    #path('evntcat/', views.Categorylist.as_view(), name='events_by_category'),
    path('<int:pk>/', views.EventtDetail.as_view(), name='event-detail'),
    path('new/', views.EventtCreate.as_view(), name='event-create' ),
    path('<int:pk>/delete', views.EventtDelete.as_view(), name='event-delete'),
    path('<int:pk>/update', views.EventtUpdate.as_view(), name='event-update'),
]

