
from django.contrib import admin

# Register your models here.
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    fields = ['name', 'slug']


admin.site.register(Category, CategoryAdmin)


class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'detail', 'date')
    fields = ('category', 'title', 'creator', 'detail', 'date')
    search_fields = ('name', 'details')
    list_filter = ['date', 'category']


admin.site.register(Eventt, EventAdmin)
